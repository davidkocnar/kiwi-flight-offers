package cz.davidkocnar.kiwiflightoffers.rest;

import android.content.Context;

import com.blankj.utilcode.util.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by David on 04.04.2018.
 * Description:
 */

public class KiwiClient {

	private static final String BASE_URL = "https://api.skypicker.com";
	private static Retrofit retrofit = null;

	static Retrofit getClient(Context context) {
		if (retrofit == null) {
			long now = getNowInSecs();
			long dayEnd = getEndOfDayInSecs();
			long toEndOfDay = dayEnd - now;

			OkHttpClient okHttpClient = new OkHttpClient.Builder()
					.cache(new Cache(new File(context.getCacheDir(), "apiResponses"), 5 * 1024 * 1024)) // max 10 MB
					.connectTimeout(10, TimeUnit.SECONDS)
					.readTimeout(10, TimeUnit.SECONDS)
					.addInterceptor(chain -> {  // Caching
						Request request = chain.request();
						if (NetworkUtils.isConnected()) {
							request = request.newBuilder().header("Cache-Control", "public, max-age=" + toEndOfDay).build();
						} else {
							request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + toEndOfDay).build();
						}

						return chain.proceed(request).newBuilder().header("Cache-Control", "public, max-age=" + toEndOfDay).build();
					})
					.build();


			Gson gson = new GsonBuilder()
					.setLenient()  // For imperfect JSON format
					.create();

			retrofit = new Retrofit.Builder()
					.baseUrl(BASE_URL)
					.client(okHttpClient)
					.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
					.addConverterFactory(GsonConverterFactory.create(gson))
					.build();
		}
		return retrofit;
	}

	private static long getNowInSecs() {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTimeInMillis()/1000;
	}

	private static long getStartOfDayInSecs() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis()/1000;
	}

	private static long getEndOfDayInSecs() {
		return getStartOfDayInSecs() + (24 * 60 * 60);
	}
}
