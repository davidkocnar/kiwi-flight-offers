package cz.davidkocnar.kiwiflightoffers.rest;

import cz.davidkocnar.kiwiflightoffers.model.FlightsResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by David on 04.04.2018.
 * Description:
 */

interface ApiInterface {

	@GET("/flights")
	Observable<FlightsResponse> getFlights(
			@Query("v") int v,
			@Query("sort") String sort,
			@Query("asc") int asc,
			@Query("locale") String locale,
			@Query("flyFrom") String flyFrom,
			@Query("dateFrom") String dateFrom,
			@Query("dateTo") String dateTo,
			@Query("oneforcity") int oneforcity,
			@Query("adults") int adults,
			@Query("partner") String partner,
			@Query("limit") int limit);

}
