package cz.davidkocnar.kiwiflightoffers.rest;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import cz.davidkocnar.kiwiflightoffers.activity.MainActivity;
import cz.davidkocnar.kiwiflightoffers.model.Flight;
import cz.davidkocnar.kiwiflightoffers.model.FlightsResponse;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by David on 04.04.2018.
 * Description:
 */

public class Repository {

	private ApiInterface apiInterface;
	private MainActivity activity;
	private LinkedHashSet<Flight> loadedFlights = new LinkedHashSet<>();
	private int requestCount = 5;

	public Repository(MainActivity activity) {
		this.activity = activity;
	}

	public void requestFlights() {
		if (apiInterface == null) apiInterface = KiwiClient.getClient(activity).create(ApiInterface.class);

		Observable<FlightsResponse> call = apiInterface.getFlights(
				2,
				"popularity",
				0,
				"en",
				"49.2-16.61-250km",  //TODO: simply add real GPS coordinates
				getTodaysDate(),
				getFutureDate(90),
				1,
				1,
				"picky",
				this.requestCount
		);

		Log.d("Repository", "### Flights requesting: ");

		call.subscribeOn(Schedulers.newThread())  // Reactive way
				.retry()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(createFlightsResponseObserver());
	}

	private Observer<FlightsResponse> createFlightsResponseObserver() {
		return new Observer<FlightsResponse>() {

			@Override
			public void onError(Throwable e) {
				Log.e("Repository", "Error while fetching data! " + e.getMessage());
			}

			@Override
			public void onComplete() {}

			@Override
			public void onSubscribe(@NonNull Disposable d) {}

			@Override
			public void onNext(FlightsResponse response) {
				if (response.getResults() != null && response.getResults() > 0) {
					Log.d("PhotosRepository", "### Flights result: count=" + response.getResults());
					List<Flight> flights = response.getData();
					List<Flight> toRemove = new ArrayList<>();

					for (int i = 0; i < response.getData().size(); i++) {
						Flight flight = response.getData().get(i);

						if (wasDisplayed(flight.getId())) {
							toRemove.add(flight);
						}
					}
					flights.removeAll(toRemove);
					loadedFlights.addAll(flights);

					if (loadedFlights.size() < 5) {
						requestCount += 5;
						requestFlights();
					}
					activity.setFlights(loadedFlights, response.getCurrency());
				}
			}
		};
	}

	private boolean wasDisplayed(String id) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
		if (!sharedPref.getString(id, getTodaysDate()).equals(getTodaysDate())) {
			Log.d("Repository", "Was loaded flight that was already displayed.");
			return true;
		}
		return false;
	}

	public void setAsDisplayed(String id) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(id, getTodaysDate());
		editor.apply();
	}

	private String getTodaysDate() {
		Date d = Calendar.getInstance().getTime();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		return df.format(d);
	}

	private String getFutureDate(int days) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.DATE, days);
		Date futureDate = calendar.getTime();
		calendar.add(Calendar.DATE, -days);

		return df.format(futureDate);
	}

}
