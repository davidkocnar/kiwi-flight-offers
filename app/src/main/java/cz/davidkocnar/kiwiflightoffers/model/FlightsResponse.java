
package cz.davidkocnar.kiwiflightoffers.model;

import android.databinding.BaseObservable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FlightsResponse extends BaseObservable implements Serializable
{

    @SerializedName("airlinesList")
    @Expose
    private List<Object> airlinesList = null;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("currency_rate")
    @Expose
    private Double currencyRate;
    @SerializedName("ref_tasks")
    @Expose
    private List<Object> refTasks = null;
    @SerializedName("search_params")
    @Expose
    private SearchParams searchParams;
    @SerializedName("hashtags")
    @Expose
    private List<Hashtag> hashtags = null;
    @SerializedName("refresh")
    @Expose
    private List<Object> refresh = null;
    @SerializedName("connections")
    @Expose
    private List<Object> connections = null;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("all_stopover_airports")
    @Expose
    private List<String> allStopoverAirports = null;
    @SerializedName("del")
    @Expose
    private Integer del;
    @SerializedName("all_airlines")
    @Expose
    private List<Object> allAirlines = null;
    @SerializedName("_results")
    @Expose
    private Integer results;
    @SerializedName("airportsList")
    @Expose
    private List<AirportsList> airportsList = null;
    @SerializedName("data")
    @Expose
    private List<Flight> data = null;
    private final static long serialVersionUID = -4560621068438026270L;

    public List<Object> getAirlinesList() {
        return airlinesList;
    }

    public void setAirlinesList(List<Object> airlinesList) {
        this.airlinesList = airlinesList;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Double getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(Double currencyRate) {
        this.currencyRate = currencyRate;
    }

    public List<Object> getRefTasks() {
        return refTasks;
    }

    public void setRefTasks(List<Object> refTasks) {
        this.refTasks = refTasks;
    }

    public SearchParams getSearchParams() {
        return searchParams;
    }

    public void setSearchParams(SearchParams searchParams) {
        this.searchParams = searchParams;
    }

    public List<Hashtag> getHashtags() {
        return hashtags;
    }

    public void setHashtags(List<Hashtag> hashtags) {
        this.hashtags = hashtags;
    }

    public List<Object> getRefresh() {
        return refresh;
    }

    public void setRefresh(List<Object> refresh) {
        this.refresh = refresh;
    }

    public List<Object> getConnections() {
        return connections;
    }

    public void setConnections(List<Object> connections) {
        this.connections = connections;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<String> getAllStopoverAirports() {
        return allStopoverAirports;
    }

    public void setAllStopoverAirports(List<String> allStopoverAirports) {
        this.allStopoverAirports = allStopoverAirports;
    }

    public Integer getDel() {
        return del;
    }

    public void setDel(Integer del) {
        this.del = del;
    }

    public List<Object> getAllAirlines() {
        return allAirlines;
    }

    public void setAllAirlines(List<Object> allAirlines) {
        this.allAirlines = allAirlines;
    }

    public Integer getResults() {
        return results;
    }

    public void setResults(Integer results) {
        this.results = results;
    }

    public List<AirportsList> getAirportsList() {
        return airportsList;
    }

    public void setAirportsList(List<AirportsList> airportsList) {
        this.airportsList = airportsList;
    }

    public List<Flight> getData() {
        return data;
    }

    public void setData(List<Flight> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "results=" + results + " searchParams=" + searchParams;
    }

}
