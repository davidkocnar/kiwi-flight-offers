
package cz.davidkocnar.kiwiflightoffers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Conversion implements Serializable
{

    @SerializedName("EUR")
    @Expose
    private Integer eUR;
    private final static long serialVersionUID = -3163714993026827422L;

    public Integer getEUR() {
        return eUR;
    }

    public void setEUR(Integer eUR) {
        this.eUR = eUR;
    }

    /*@Override
    public String toString() {
        return new ToStringBuilder(this).append("eUR", eUR).toString();
    }
*/
}
