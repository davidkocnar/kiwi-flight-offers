
package cz.davidkocnar.kiwiflightoffers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AirportsList implements Serializable
{

    @SerializedName("filterName")
    @Expose
    private String filterName;
    @SerializedName("name")
    @Expose
    private String name;
    private final static long serialVersionUID = 9080278078931684221L;

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*@Override
    public String toString() {
        return new ToStringBuilder(this).append("filterName", filterName).append("name", name).toString();
    }*/

}
