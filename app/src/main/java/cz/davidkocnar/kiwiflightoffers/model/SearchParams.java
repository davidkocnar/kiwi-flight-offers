
package cz.davidkocnar.kiwiflightoffers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SearchParams implements Serializable
{

    @SerializedName("to_type")
    @Expose
    private String toType;
    @SerializedName("flyFrom_type")
    @Expose
    private String flyFromType;
    @SerializedName("seats")
    @Expose
    private Seats seats;
    private final static long serialVersionUID = 2516360061146108005L;

    public String getToType() {
        return toType;
    }

    public void setToType(String toType) {
        this.toType = toType;
    }

    public String getFlyFromType() {
        return flyFromType;
    }

    public void setFlyFromType(String flyFromType) {
        this.flyFromType = flyFromType;
    }

    public Seats getSeats() {
        return seats;
    }

    public void setSeats(Seats seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "toType=" + toType + " flyFromType=" + flyFromType;
    }

}
