
package cz.davidkocnar.kiwiflightoffers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BagsPrice implements Serializable
{

    @SerializedName("1")
    @Expose
    private Double _1;
    @SerializedName("2")
    @Expose
    private Double _2;
    private final static long serialVersionUID = -3656378150884864646L;

    public Double get1() {
        return _1;
    }

    public void set1(Double _1) {
        this._1 = _1;
    }

    public Double get2() {
        return _2;
    }

    public void set2(Double _2) {
        this._2 = _2;
    }

    /*@Override
    public String toString() {
        return new ToStringBuilder(this).append("_1", _1).append("_2", _2).toString();
    }*/

}
