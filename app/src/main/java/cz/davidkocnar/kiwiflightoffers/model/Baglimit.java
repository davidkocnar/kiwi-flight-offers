
package cz.davidkocnar.kiwiflightoffers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Baglimit implements Serializable
{

    @SerializedName("hand_width")
    @Expose
    private Integer handWidth;
    @SerializedName("hand_length")
    @Expose
    private Integer handLength;
    @SerializedName("hand_weight")
    @Expose
    private Integer handWeight;
    @SerializedName("hand_height")
    @Expose
    private Integer handHeight;
    @SerializedName("hold_weight")
    @Expose
    private Integer holdWeight;
    private final static long serialVersionUID = 4061068772437737888L;

    public Integer getHandWidth() {
        return handWidth;
    }

    public void setHandWidth(Integer handWidth) {
        this.handWidth = handWidth;
    }

    public Integer getHandLength() {
        return handLength;
    }

    public void setHandLength(Integer handLength) {
        this.handLength = handLength;
    }

    public Integer getHandWeight() {
        return handWeight;
    }

    public void setHandWeight(Integer handWeight) {
        this.handWeight = handWeight;
    }

    public Integer getHandHeight() {
        return handHeight;
    }

    public void setHandHeight(Integer handHeight) {
        this.handHeight = handHeight;
    }

    public Integer getHoldWeight() {
        return holdWeight;
    }

    public void setHoldWeight(Integer holdWeight) {
        this.holdWeight = holdWeight;
    }

    /*@Override
    public String toString() {
        return new ToStringBuilder(this).append("handWidth", handWidth).append("handLength", handLength).append("handWeight", handWeight).append("handHeight", handHeight).append("holdWeight", holdWeight).toString();
    }*/

}
