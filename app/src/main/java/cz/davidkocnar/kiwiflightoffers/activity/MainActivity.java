package cz.davidkocnar.kiwiflightoffers.activity;

import android.animation.LayoutTransition;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.Utils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import cz.davidkocnar.kiwiflightoffers.BR;
import cz.davidkocnar.kiwiflightoffers.R;
import cz.davidkocnar.kiwiflightoffers.databinding.ActivityMainBinding;
import cz.davidkocnar.kiwiflightoffers.databinding.FragmentMainBinding;
import cz.davidkocnar.kiwiflightoffers.model.Flight;
import cz.davidkocnar.kiwiflightoffers.rest.Repository;

public class MainActivity extends AppCompatActivity {

	private SectionsPagerAdapter mSectionsPagerAdapter;

	private ViewPager mViewPager;
	public List<Flight> flights;
	private Repository repository;
	private ActivityMainBinding binding;
	public String currency;
	private ImageView[] indicators = new ImageView[5];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		mViewPager = binding.container;
		indicators[0] = binding.introIndicator0;
		indicators[1] = binding.introIndicator1;
		indicators[2] = binding.introIndicator2;
		indicators[3] = binding.introIndicator3;
		indicators[4] = binding.introIndicator4;

		binding.mainContent.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
		binding.container.setAdapter(mSectionsPagerAdapter);

		Utils.init(this);
		repository = new Repository(this);
		repository.requestFlights();

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(view -> {
			int curr = mViewPager.getCurrentItem();
			if (curr <= flights.size()) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(flights.get(curr).getDeepLink()));
				startActivity(browserIntent);
			}
			Snackbar.make(view, "Opening link to Kiwi.com", Snackbar.LENGTH_LONG)
					.setAction("Action", null).show();
		});

		mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
			@Override
			public void onPageScrollStateChanged(int state) {}

			@Override
			public void onPageSelected(int position) {
				for (int i = 0; i < indicators.length; i++) {
					indicators[i].setBackgroundResource(
							i == position ? R.drawable.dot_indicator_selected : R.drawable.dot_indicator_unselected
					);
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	public void setFlights(LinkedHashSet<Flight> data, String currency) {
		this.flights = new ArrayList<>(data);
		this.currency = currency;
		if (this.flights.size() > 5) this.flights.subList(5, this.flights.size()).clear();

		if (this.flights.size() > 0) {
			binding.progress.setVisibility(View.GONE);
			binding.progressLabel.setVisibility(View.GONE);
			binding.fab.setVisibility(View.VISIBLE);
			mSectionsPagerAdapter.notifyDataSetChanged();
		}
	}


	private class SectionsPagerAdapter extends FragmentPagerAdapter {

		SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			if (flights != null && flights.size() > 0) {
				repository.setAsDisplayed(flights.get(position).getId());
				return PlaceholderFragment.newInstance(flights.get(position), currency, position + 1);
			}
			else {
				return PlaceholderFragment.newInstance(null, currency, position + 1);
			}
		}

		@Override
		public int getCount() {
			return flights == null? 0: flights.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "FLIGHT " + (position+1);
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		private static final String ARG_SECTION_NUMBER = "section_number";
		private static final String FLIGHT_EXTRA = "FLIGHT_EXTRA";
		private static final String CURRENCY_EXTRA = "CURRENCY_EXTRA";
		Flight flight;
		int pageNum;
		FragmentMainBinding binding;


		/**
		 * Returns a new instance of this fragment for the given section
		 * number.
		 */
		public static PlaceholderFragment newInstance(Flight flight, String currency, int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putSerializable(FLIGHT_EXTRA, flight);
			args.putSerializable(CURRENCY_EXTRA, currency);
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			flight = (Flight) getArguments().getSerializable(FLIGHT_EXTRA);
			String currency = getArguments().getString(CURRENCY_EXTRA);
			pageNum = getArguments().getInt(ARG_SECTION_NUMBER);
			binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
			View rootView = binding.getRoot();

			if (flight != null) {
				Log.d("PlaceholderFragment", "Flight page=" + pageNum + " id=" + flight.getId());
				binding.setFlight(flight);
				binding.notifyPropertyChanged(BR.flight);
				binding.sectionPrice.setText(flight.getPrice() + " " + currency);

				addInfoItemToLayout("From:", flight.getCityFrom(), R.drawable.ic_flight_takeoff_white_24dp);
				addInfoItemToLayout("Date:", flight.getFormatedDTime(), R.drawable.ic_date_range_black_24dp);
				addInfoItemToLayout("Fly duration:", flight.getFlyDuration(), R.drawable.ic_access_time_black_24dp);
				addInfoItemToLayout("Routes:", flight.getPreparedRoutes(), R.drawable.ic_public_black_24dp);
				addInfoItemToLayout("Stopovers:", String.valueOf(flight.getRoute().size() - 1), R.drawable.ic_swap_horiz_black_24dp);
				addInfoItemToLayout("Facilitated booking available:",
						flight.getFacilitatedBookingAvailable() ? "Yes" : "No", R.drawable.ic_tag_faces_black_24dp);
			}
			return rootView;
		}

		private void addInfoItemToLayout(String label, String value, int resourceId) {
			LinearLayout infoItemLayout = (LinearLayout) getLayoutInflater(null).inflate(R.layout.info_item, null);
			TextView labelTV = (TextView) infoItemLayout.findViewById(R.id.section_info_label);
			labelTV.setText(label);
			TextView valueTV = (TextView) infoItemLayout.findViewById(R.id.section_info);
			valueTV.setText(value);
			ImageView iconIV = (ImageView) infoItemLayout.findViewById(R.id.icon);
			iconIV.setBackgroundResource(resourceId);

			String imageUri = getResources().getString(R.string.image_url, flight.getMapIdto());
			Log.d("PlaceholderFragment", "Image URI=" + imageUri);
			Glide.with(getContext())
					.load(imageUri)
					.into(binding.flightImage);

			binding.contentLayout.addView(infoItemLayout);
		}
	}
}
